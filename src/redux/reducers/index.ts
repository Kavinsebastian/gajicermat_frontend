import { combineReducers } from "redux";
import rootReducer from "./rootReducer";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

const persistConfig = {
  key: "response",
  storage,
  whitelist: ["wishlist"],
};

const Reducer = combineReducers({
  wishlist: rootReducer,
});

export default persistReducer(persistConfig, Reducer);
