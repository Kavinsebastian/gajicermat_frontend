import { createStore } from "redux";
import Reducers from "./reducers";

import { persistStore } from "redux-persist";

export const store = createStore(Reducers);

export const persistor = persistStore(store);
