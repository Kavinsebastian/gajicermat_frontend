import { useSelector } from "react-redux";
import "./css/Wishlist.css";
import styled from "styled-components";
import Pagination from "../comp/Pagination";
import { useState } from "react";
import Navbar from "../comp/Navbar";
import { motion } from "framer-motion";

export default function Wishlist() {
  const wishlist = useSelector((state: any) => state.wishlist.planets);
  const [currentPage, setCurrentPage] = useState(1);
  const [postPerPage] = useState(3);

  // Get current post
  const indexOfLastPost = currentPage * postPerPage;
  const indexOfFirstPost = indexOfLastPost - postPerPage;
  const currentPost = wishlist.slice(indexOfFirstPost, indexOfLastPost);

  // function for page
  const paginate = (pageNumber: any) => setCurrentPage(pageNumber);

  // style
  const Flexbox = styled.div`
    display: flex;
    width: auto;
    justify-content: center;
    align-items: center;
  `;

  const TextBody = styled.h1`
    font-size: 1.5rem;
    font-weight: bold;
    font-family: Arial, Helvetica, sans-serif;
    max-width: max-content;
    @media (max-width: 1280px) {
      font-size: 1.2rem;
    }
    @media (max-width: 1024px) {
      font-size: 0.9rem;
    }
  `;

  const TextHead = styled.h1`
    font-size: 2rem;
    font-weight: bold;
    font-family: Arial, Helvetica, sans-serif;
    text-align: center;
    padding: 50px 0;
    color: white;
  `;

  return (
    <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }} transition={{ duration: 2 }}>
      <div className='bg-wishlist'></div>
      <Navbar />
      <TextHead>Wishlist</TextHead>
      {currentPost.length > 0 ? (
        currentPost.map((planet: any, key: any) => (
          <div key={key} className='card'>
            <img src='/assets/146054-OTSKBF-217.jpg' alt='' />
            <div className='text-body'>
              <TextBody>Planet: {planet.name}</TextBody>
              <TextBody>Rotation-period: {planet.orbital_periode}</TextBody>
              <TextBody>Orbital-period: {planet.orbital_periode} </TextBody>
              <TextBody>diameter: {planet.diameter} </TextBody>
              <TextBody>Climate: {planet.climate} </TextBody>
              <TextBody>Gravity: {planet.gravity} </TextBody>
              <TextBody>Terrain: {planet.terrain} </TextBody>
              <TextBody>Surface-wate: {planet.surface_water} </TextBody>
              <TextBody>Population: {planet.population} </TextBody>
            </div>
          </div>
        ))
      ) : (
        <Flexbox>
          <h1>Data tidak ditemukan</h1>
        </Flexbox>
      )}

      <Pagination totalPost={wishlist.length} postPerPage={postPerPage} paginate={paginate} />
    </motion.div>
  );
}
