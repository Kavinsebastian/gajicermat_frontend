import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
import "./css/Detail.css";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { addNewWishlist } from "../../redux/action";
import { motion } from "framer-motion";
import Navbar from "../comp/Navbar";

export default function Detail() {
  // style
  const TextHead = styled.h1`
    font-size: 5rem;
    font-weight: bold;
    font-family: Arial, Helvetica, sans-serif;
    color: white;
  `;

  // fetching data rest API and Redux store
  const { created }: any = useParams();
  const [allPlanets, setAllPlanets] = useState([] as any);
  const data = allPlanets.find((planet: any) => planet.created === created);
  const wishlist = useSelector((state: any) => state.wishlist.planets);
  const dispatch = useDispatch();
  console.log("wish", wishlist);

  useEffect(() => {
    fetchDataPlanets1();
    fetchDataPlanets2();
    fetchDataPlanets3();
    fetchDataPlanets4();
    fetchDataPlanets5();
    fetchDataPlanets6();
  }, [data]);

  // add data to wishlist
  const handleAddWishlist = () => {
    const planet = {
      name: data.name,
      rotation_periode: data.rotation_period,
      orbital_periode: data.orbital_period,
      diameter: data.diameter,
      climate: data.climate,
      gravity: data.gravity,
      terrain: data.terrain,
      surface_water: data.surface_water,
      population: data.population,
    };

    for (let i = 0; i < wishlist.length; i++) {
      if (planet.name === wishlist[i].name) {
        return false;
      }
    }
    dispatch(addNewWishlist(planet));
    localStorage.setItem("response", JSON.stringify(planet));
  };

  const fetchDataPlanets1 = async () => {
    await axios.get("https://swapi.dev/api/planets").then(planet => {
      const planets: never[] = planet.data.results;
      setAllPlanets((allPlanets: any) => [...allPlanets, ...planets]);
    });
  };

  const fetchDataPlanets2 = async () => {
    await axios.get("https://swapi.dev/api/planets/?page=2").then(planet => {
      const planets: never[] = planet.data.results;
      setAllPlanets((allPlanets: any) => [...allPlanets, ...planets]);
    });
  };

  const fetchDataPlanets3 = async () => {
    await axios.get("https://swapi.dev/api/planets/?page=3").then(planet => {
      const planets: never[] = planet.data.results;
      setAllPlanets((allPlanets: any) => [...allPlanets, ...planets]);
    });
  };
  const fetchDataPlanets4 = async () => {
    await axios.get("https://swapi.dev/api/planets/?page=4").then(planet => {
      const planets: never[] = planet.data.results;
      setAllPlanets((allPlanets: any) => [...allPlanets, ...planets]);
    });
  };
  const fetchDataPlanets5 = async () => {
    await axios.get("https://swapi.dev/api/planets/?page=5").then(planet => {
      const planets: never[] = planet.data.results;
      setAllPlanets((allPlanets: any) => [...allPlanets, ...planets]);
    });
  };
  const fetchDataPlanets6 = async () => {
    await axios.get("https://swapi.dev/api/planets/?page=6").then(planet => {
      const planets: never[] = planet.data.results;
      setAllPlanets((allPlanets: any) => [...allPlanets, ...planets]);
    });
  };

  return (
    <motion.div
      className='bg-detail'
      initial={{ width: "100%", height: "150vh", opacity: 0.5 }}
      animate={{ width: "100%", height: "110vh", opacity: 1 }}
      exit={{ scale: 2, opacity: 0 }}
      transition={{ duration: 3 }}>
      <Navbar />
      {data ? (
        <>
          <div className='title'>
            <TextHead>{data.name}</TextHead>
          </div>
          <div className='content'>
            <p style={{ width: "200px", color: "white" }}>
              Tap or Drag for add to wishlist
              <motion.div drag='x' style={{ width: "50px" }}>
                <motion.img drag='y' whileDrag={{ scale: 1.5 }} onClick={handleAddWishlist} className='wishlist-icon' src='https://img.icons8.com/nolan/64/filled-like.png' alt='love' />
              </motion.div>
            </p>
            <ul>
              <li>Rotation-periode: {data.rotation_period}</li>
              <li>Orbital-periode: {data.orbital_period} </li>
              <li>Diameter: {data.diameter} </li>
              <li>Climate: {data.climate} </li>
              <li>Gravity: {data.gravity} </li>
              <li>Terrain: {data.terrain} </li>
              <li>Surface-water: {data.surface_water} </li>
              <li>population: {data.population} </li>
            </ul>
          </div>
        </>
      ) : (
        <>
          <div className='title'>
            <h1>Please wait..</h1>
          </div>
        </>
      )}
    </motion.div>
  );
}
