const initialState = {
  planets: [],
};

console.log("initial", initialState);

const rootReducer: any = (state: any = initialState, action: any) => {
  switch (action.type) {
    case "ADD":
      return {
        ...state,
        planets: [...state.planets, action.payload],
      };
    default:
      return state;
  }
};

export default rootReducer;
