import { Link } from "react-router-dom";

export default function Navbar() {
  return (
    <nav>
      <div id='logo'>
        <h1>Antariksa.</h1>
      </div>
      <ul className='navbar'>
        <li>
          <Link to='/'>Home</Link>
        </li>
        <li>
          <Link to='/wishlist'>Wishlist</Link>
        </li>
      </ul>
    </nav>
  );
}
