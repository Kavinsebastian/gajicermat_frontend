import { Route, Routes, useLocation } from "react-router-dom";
import Detail from "./Components/page/Detail";
import Home from "./Components/page/Home";
import Wishlist from "./Components/page/Wishlist";
import { AnimatePresence } from "framer-motion";

function App() {
  const location = useLocation();

  return (
    <AnimatePresence exitBeforeEnter>
      <Routes location={location} key={location.pathname}>
        <Route path='/' element={<Home />} />
        <Route path='/detail/:created' element={<Detail />} />
        <Route path='/wishlist' element={<Wishlist />} />
      </Routes>
    </AnimatePresence>
  );
}

export default App;
