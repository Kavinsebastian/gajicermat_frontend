import React from "react";
import ListPlanet from "../comp/ListPlanet";
import "./css/Home.css";
import { motion } from "framer-motion";
import Navbar from "../comp/Navbar";

export default function Home() {
  return (
    <motion.section initial={{ opacity: 0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }} transition={{ duration: 2 }}>
      <div className='bg-content'>
        <Navbar />
      </div>
      <ListPlanet />
    </motion.section>
  );
}
