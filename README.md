# Technical-Test Frontend

project ini adalah salah satu seleksi dari gaji cermat untuk bisa bekerja di bagian frontend.

# Rules

ada beberapa rules yang harus di perhatikan

### 1.Test Case
- Buat Daftar Tak Terbatas dari semua Data Planet
- page ini harus memiliki 3 halaman 
    -  Daftar planet

        daftar semua planet dengan gulir tak terbatas dan daftar virtual.

    -  Detail planet

        - daftar planet rinci
        - endpoint ini harus berasal dari createdDate data
        - button untuk menambahkan ke wishlist

    -  Wishlist planet

        - daftar planet di wishlist harus menggunakan pagination

### 2. Tech Stack
- Harus menggunakan TypeScript
- Harus menggunakan ReactJS atau ReactNative

### 3. Requiremets
- menggunakan React Hook
- Git
- bebas menggunakan Rest API atau GraphQL
- harus menggunakan CSS di JS (Styled-Components)
- menggunakan linter (ESLint)
- hindari penggunaan dependency yang terlalu besar (momen/lodash)
- initialisasi menggunakan "create-react-app" atau "create-react-native-app"
- menggunakan react testing adalah nilai plus yang besar
- bebas untuk penggunaan UI



##### Hambatan dan Rintangan (pribadi)

ada beberapa hambatan dan rintangan yang saya hadapi untuk menyelesaikan test ini
mulai dari koneksi internet sampai rest API yang tiba-tiba tidak dapat di akses
1. koneksi internet

     selama ini saya menggunakan koneksi internet menggunakan hotspot hp yang kadang notifikasi kuota tiba-tiba habis :(

2. laptop

      jadi sebenarnya laptop saya untuk saat ini masih mumpuni untuk ngoding tetapi kadang kala saya hanya bisa di pakai untuk malam hari

     karena di pakai oleh adik saya yang sedang kuliah.

3. minim pengalaman

     jujur ini rintangan yang paling berat bagi saya karena bingung mau cari dimana dan juga kadang insecure karena tidak bisa kuliah :( , tetapi bukan jadi hambatan bagi saya untuk tidak menjadi lebih baik :) (sedikit curhat wkwk)

4. TypeScript & styled-components

     sebenarnya saya belum pernah menggunakan typescript dan styled-components tetapi dengan membaca documentasi dan 
     video tutorial (tutorial hell huhu...) sedikit dapat di pahami untuk di kerjakan.

5. react testing

     tidak saya kerjakan karena saya belum mengetahui, mungkin saya noted! untuk list pembelajaran di lain waktu .

6. rest API tidak bisa di akses

     sekarang new Date() rest API gak bisa di akses , dan kodingan saya tinggal mempercantik UI pengen nya sih 
     
     mau coba pakai framer-motion dan juga pagination belum di terapin :(

7. virtualized-list
     
     gatau kenapa saya stuck untuk membuat virtualized-list sudah mencoba salah satu package manager tetap error

     dan error nya itu dikarenakan versi typescript nya sedangkan saya menggunakan npm-cli create-react-app --template typescript.


mungkin itulah rintangan dan hambatan saya saat ini mohon maaf bila kurang maksimal dan terima kasih sudah memberikan kesempatan 

untuk menunjukan hasil dari saya, Terima Kasih.





