import axios from "axios";
import { useEffect, useState } from "react";
import styled from "styled-components";
import InfiniteScroll from "react-infinite-scroll-component";
import "./css/ListPlanet.css";
import { motion } from "framer-motion";
import { Link } from "react-router-dom";

export default function ListPlanet() {
  // style
  const TextHead = styled.h1`
    font-size: 2rem;
    font-weight: bold;
    font-family: "Times New Roman", Times, serif;
  `;

  const TextBody = styled.p`
    font-size: 2rem;
    margin: 5vh;
    color: white;
    font-weight: bold;
  `;

  // data

  const [allPlanets, setAllPlanets] = useState([] as any);
  const [page, setPage] = useState(1);

  useEffect(() => {
    fetchDataPlanets();
  }, [page]);

  const fetchDataPlanets = async () => {
    await axios
      .get(`https://swapi.dev/api/planets/?page=${page}`)
      .then(planet => {
        // console.log(planet.data.results);
        const planets: never[] = planet.data.results;
        setAllPlanets((allPlanets: any) => [...allPlanets, ...planets]);
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <motion.div id='list-planets'>
      <TextHead>List Planets</TextHead>
      <InfiniteScroll dataLength={allPlanets.length} next={() => setPage(page + 1)} loader={<p>please wait...</p>} hasMore={true}>
        {allPlanets.map((planet: any, key: any) => {
          return (
            <div key={key} className='list-box'>
              <Link to={`/detail/${planet.created}`}>
                <motion.div className='box' initial={{ width: "50%", height: "20vh" }} exit={{ width: "100%", height: "150vh", opacity: 0.5 }} transition={{ duration: 3 }}>
                  <TextBody>{planet.name}</TextBody>
                </motion.div>
              </Link>
            </div>
          );
        })}
      </InfiniteScroll>
    </motion.div>
  );
}
