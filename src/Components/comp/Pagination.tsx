import styled from "styled-components";
export default function Pagination({ totalPost, postPerPage, paginate }: any) {

  // for button pagination
  const pageNumber = [];
  for (let i = 1; i <= Math.ceil(totalPost / postPerPage); i++) {
    pageNumber.push(i);
  }

  // style
  const Ul = styled.ul`
    padding: 10px;
    display: flex;
    justify-content: center;
    text-align: center;
  `;
  const Li = styled.li`
    margin: 0 6px;
    list-style: none;
    width: 20px;
    height: 20px;
    border: 3px solid #cccaca;
    color: aquamarine;
    padding: 10px;
    transition: 0.3s;
    cursor: pointer;
    &:hover {
      transform: translateY(-10px);
      background-color: #d5d5d5;
      color: black;
    }
  `;

  return (
    <div>
      <Ul>
        {pageNumber ? (
          pageNumber.map((number: any, key: any) => (
            <Li onClick={() => paginate(number)} key={key}>
              {number}
            </Li>
          ))
        ) : (
          <h1>loading</h1>
        )}
      </Ul>
    </div>
  );
}
